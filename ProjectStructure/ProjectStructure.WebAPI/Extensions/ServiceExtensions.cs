﻿using Microsoft.Extensions.DependencyInjection;
using ProjectStructure.BLL.Services;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.DAL.Repositories;
using ProjectStructure.DAL.Repositories.Abstract;

namespace ProjectStructure.WebAPI.Extensions
{
    public static class ServiceExtensions
    {
        public static void RegisterRepositories(this IServiceCollection services)
        {
            services.AddScoped<IProjectsRepository, ProjectsRepository>();
            services.AddScoped<ITasksRepository, TasksRepository>();
            services.AddScoped<ITeamsRepository, TeamsRepository>();
            services.AddScoped<IUsersRepository, UsersRepository>();
        }

        public static void RegisterCustomServices(this IServiceCollection services)
        {
            services.AddScoped<IProjectsService, ProjectsService>();
            services.AddScoped<ITasksService, TasksService>();
            services.AddScoped<ITeamsService, TeamsService>();
            services.AddScoped<IUsersService, UsersService>();
        }
    }
}
