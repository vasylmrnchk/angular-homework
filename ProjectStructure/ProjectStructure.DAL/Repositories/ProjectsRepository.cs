﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.DAL.Repositories
{
    public class ProjectsRepository : BasicRepository<Entities.Project>, IProjectsRepository
    {
        public ProjectsRepository(ProjectStructureDbContext context) : base(context)
        {

        }

        public override async Task<Entities.Project> CreateAsync(Entities.Project entity)
        {
            await CheckAuthorAndTeam(entity);
            return await base.CreateAsync(entity);
        }

        public override async Task<IEnumerable<Entities.Project>> ReadAllAsync()
        {
            return await _context.Set<Entities.Project>()
                                    .Include(p => p.Author)
                                    .Include(p => p.Team)
                                    .Include(p => p.Tasks)
                                        .ThenInclude(t => t.Performer).ToListAsync();
        }

        public override async Task<Entities.Project> ReadAsync(int id)
        {
            return await _context.Projects
                                    .Include(p => p.Author)
                                    .Include(p => p.Team)
                                    .Include(p => p.Tasks)
                                        .ThenInclude( t=> t.Performer)
                                    .FirstOrDefaultAsync(p => p.Id == id) ?? 
                                                            throw new KeyNotFoundException($"Project not found.");
        }

        public override async Task UpdateAsync(Entities.Project entity)
        {
            await CheckAuthorAndTeam(entity);
            await base.UpdateAsync(entity);
        }

        private async Task CheckAuthorAndTeam(Entities.Project entity)
        {
            if (await _context.Set<Entities.Team>().FindAsync(entity.TeamId) is null ||
                await _context.Set<Entities.User>().FindAsync(entity.AuthorId) is null)
                throw new ArgumentException("Invalid AuthorId or TeamId");
        }
    }
}
