﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.DAL.Repositories
{
    public class UsersRepository : BasicRepository<Entities.User>, IUsersRepository
    {
        public UsersRepository(ProjectStructureDbContext context) : base(context)
        {

        }

        public override async Task<Entities.User> CreateAsync(Entities.User entity)
        {
            await CheckTeam(entity.TeamId);
            return await base.CreateAsync(entity);
        }

        public override async Task<IEnumerable<Entities.User>> ReadAllAsync()
        {
            return await _context.Set<Entities.User>()
                        .Include(u => u.Team).ToListAsync();
        }

        public override async Task<Entities.User> ReadAsync(int id)
        {
            return await _context.Users.Include(u => u.Team)
                                        .FirstOrDefaultAsync(u => u.Id == id) ?? 
                                            throw new KeyNotFoundException($"User not found.");

        }

        public override async Task UpdateAsync(Entities.User entity)
        {
            await CheckTeam(entity.TeamId);
            await base.UpdateAsync(entity);
        }

        private async Task CheckTeam(int? id)
        {
            if (await _context.Set<Entities.Team>().FindAsync(id.GetValueOrDefault()) is null && id.HasValue)
                throw new ArgumentException("Invalid TeamId");
        }
    }
}
