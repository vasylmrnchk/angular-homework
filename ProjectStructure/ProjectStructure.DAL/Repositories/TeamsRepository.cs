﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Repositories.Abstract;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.DAL.Repositories
{
    public class TeamsRepository : BasicRepository<Team>, ITeamsRepository
    {
        public TeamsRepository(ProjectStructureDbContext context) : base(context)
        {
        }

        public override async Task<IEnumerable<Team>> ReadAllAsync()
        {
            return await _context.Set<Entities.Team>().ToListAsync();
        }

        public override async Task<Team> ReadAsync(int id)
        {
            return await _context.Teams.FirstOrDefaultAsync(t => t.Id == id) ?? 
                                            throw new KeyNotFoundException($"Team not found.");

        }
    }
}
