﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.DAL.Repositories
{
    public class TasksRepository : BasicRepository<Entities.Task>, ITasksRepository
    {
        public TasksRepository(ProjectStructureDbContext context) : base(context)
        {
        }

        public override async Task<Entities.Task> CreateAsync(Entities.Task entity)
        {
            await CheckPerformerAndProject(entity);
            return await base.CreateAsync(entity);
        }

        public override async Task<IEnumerable<Entities.Task>> ReadAllAsync()
        {
            return await _context.Set<Entities.Task>()
                        .Include(t => t.Performer).ToListAsync();
        }

        public override async Task<Entities.Task> ReadAsync(int id)
        {
            return await _context.Tasks.Include(t => t.Performer)
                                        .FirstOrDefaultAsync(t => t.Id == id) ?? 
                                            throw new KeyNotFoundException($"Task not found.");
        }

        public override async Task UpdateAsync(Entities.Task entity)
        {
            var edited = await GetByIdAsync(entity.Id);
            if (edited.State == TaskState.Completed)
            {
                throw new ArgumentException($"Error! Task wtih id={edited.Id} already completed.");
            }
            entity.CreatedAt = edited.CreatedAt;
            entity.PerformerId = edited.PerformerId;
            entity.ProjectId = edited.ProjectId;
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        private async Task CheckPerformerAndProject(Entities.Task entity)
        {
            if (await _context.Set<Entities.Project>().FindAsync(entity.ProjectId) is null ||
                await _context.Set<Entities.User>().FindAsync(entity.PerformerId) is null)
                throw new ArgumentException("Invalid PerformerId or TeamId");
        }
    }
}
