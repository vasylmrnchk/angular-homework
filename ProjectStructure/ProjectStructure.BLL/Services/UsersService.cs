﻿using AutoMapper;
using ProjectStructure.BLL.DTO.User;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Repositories.Abstract;

namespace ProjectStructure.BLL.Services
{
    public class UsersService : BaseService<User, UserDTO, NewUserDTO, UpdateUserDTO>, IUsersService
    {
        public UsersService(IUsersRepository repository, IMapper mapper) : base(repository, mapper)
        {

        }
    }
}
