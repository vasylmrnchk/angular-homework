﻿using ProjectStructure.BLL.DTO.Team;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.BLL.Services.Abstract
{
    public interface ITeamsService : IBaseService<Team, TeamDTO, NewTeamDTO, UpdateTeamDTO>
    {
    }
}
