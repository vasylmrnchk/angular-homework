﻿using ProjectStructure.BLL.DTO.Project;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.BLL.Services.Abstract
{
    public interface IProjectsService : IBaseService<Project, ProjectDTO, NewProjectDTO, UpdateProjectDTO>
    {
    }
}
