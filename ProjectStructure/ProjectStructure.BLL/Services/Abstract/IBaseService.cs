﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services.Abstract
{
    public interface IBaseService<T, TDto, TNewDto, TUpdateDTo>
    {
        Task<IEnumerable<TDto>> ReadAllAsync();
        Task<TDto> ReadAsync(int id);
        Task<TDto> CreateAsync(TNewDto entityDto);
        Task UpdateAsync(TUpdateDTo entityDto);
        Task DeleteAsync(int id);
    }
}
