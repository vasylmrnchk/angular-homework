﻿using ProjectStructure.BLL.DTO.Task;
using ProjectStructure.BLL.DTO.Team;
using ProjectStructure.BLL.DTO.User;
using System;
using System.Collections.Generic;

namespace ProjectStructure.BLL.DTO.Project
{
    public class ProjectDTO
    {
        public int Id { get; set; }

        public int AuthorId { get; set; }
        public UserDTO Author { get; set; }

        public int TeamId { get; set; }
        public TeamDTO Team { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }

        public List<TaskDTO> Tasks { get; private set; } = new();
    }
}
