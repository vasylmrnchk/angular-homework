﻿namespace ProjectStructure.BLL.DTO
{
    public enum TaskStateDTO
    {
        Created,
        Started,
        Completed,
        Canceled
    }
}
