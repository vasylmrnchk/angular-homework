﻿using ProjectStructure.BLL.DTO.Project;
using ProjectStructure.BLL.DTO.User;
using System;

namespace ProjectStructure.BLL.DTO.Task
{
    public class TaskDTO
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }

        public int PerformerId { get; set; }
        public UserDTO Performer { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public TaskStateDTO State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
