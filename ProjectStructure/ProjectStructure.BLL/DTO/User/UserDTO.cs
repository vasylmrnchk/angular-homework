﻿using ProjectStructure.BLL.DTO.Team;
using System;

namespace ProjectStructure.BLL.DTO.User
{
    public class UserDTO
    {
        public int Id { get; set; }
        public int? TeamId { get; set; }
        public TeamDTO Team { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime BirthDay { get; set; }
        public DateTime RegisteredAt { get; set; }
    }
}
