﻿namespace ProjectStructure.BLL.DTO.Team
{
    public class UpdateTeamDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
