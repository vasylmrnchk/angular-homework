﻿using CollectionsAndLinqHomework.Models.User;

namespace CollectionsAndLinqHomework.DTOs
{
    class UserInfoDTO
    {
        public User User { get; set; }
        public ProjectDTO LastProject { get; set; }
        public int LastProjectTasksNumber { get; set; }
        public int UnfinishedTasksNumber { get; set; }
        public TaskDTO LongestTask { get; set; }

        public override string ToString()
        {
            return $"User: {User}\r\n\r\nLast project:\r\n\t{LastProject}\r\nTasks quantity in last project : {LastProjectTasksNumber}" +
                $"\r\nUnfinished or canceled Tasks quantity: {UnfinishedTasksNumber}\r\nLongest task:\r\n\t{LongestTask}";
        }
    }
}
