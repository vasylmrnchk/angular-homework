﻿using CollectionsAndLinqHomework.Models.User;
using System.Collections.Generic;

namespace CollectionsAndLinqHomework.DTOs
{
    class TeamDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<User> Users { get; set; }
    }
}
