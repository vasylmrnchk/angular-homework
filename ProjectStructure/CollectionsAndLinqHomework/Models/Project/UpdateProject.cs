﻿using System;

namespace CollectionsAndLinqHomework.Models.Project
{
    public class UpdateProject
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public override string ToString()
        {
            return $"{Name}";
        }
    }
}
