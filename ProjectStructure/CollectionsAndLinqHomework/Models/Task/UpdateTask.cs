﻿using System;

namespace CollectionsAndLinqHomework.Models.Task
{
    public class UpdateTask
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskState State { get; set; }
        public DateTime? FinishedAt { get; set; }
        public override string ToString()
        {
            return $"{Name}";
        }
    }
}
