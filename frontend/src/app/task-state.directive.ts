import { Directive, HostBinding, Input } from '@angular/core';
import { TaskState } from './models/common/taskState';


@Directive({
  selector: '[appTaskState]'
})
export class TaskStateDirective {
  @Input() state: number = 0;
  @HostBinding('style.backgroundColor') bgColor:string ='white';;
  constructor() { }

  ngOnChanges(){
    this.changeColor();
  }

  changeColor() {
   switch(this.state){
      case TaskState.Created:
        this.bgColor = 'gray';
      break;
      case TaskState.Started:
        this.bgColor = 'royalblue';
      break;
      case TaskState.Completed:
        this.bgColor = 'green';
      break;
        case TaskState.Canceled:
        this.bgColor = 'red';
      break;
    }
  }
}
