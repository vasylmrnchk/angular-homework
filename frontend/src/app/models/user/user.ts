import { Team } from "../team/team";

export interface User {
  id: number;
  teamId: number;
  team: Team;
  firstName: string;
  lastName: string;
  email: string;
  birthDay: Date;
  registeredAt: Date;
}