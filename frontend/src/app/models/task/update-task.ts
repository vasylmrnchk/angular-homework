import { TaskState  } from '../common/taskState';

export interface UpdateTask {
  id: number;
  name: string;
  description: string;
  state: TaskState;
  finishedAt: Date;
}