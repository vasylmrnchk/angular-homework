import { TaskState  } from '../common/taskState';
import { User } from '../user/user';

export interface Task {
  id: number;
  projectId: number;
  performerId: number;
  performer: User;
  name: string;
  description: string;
  state: TaskState;
  createdAt: Date;
  finishedAt: Date;
}