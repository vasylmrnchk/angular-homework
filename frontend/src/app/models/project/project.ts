import { Task } from "../task/task";
import { Team } from "../team/team";
import { User } from "../user/user";

export interface Project {
  id: number;
  authorId: number;
  author: User;
  teamId: number;
  team: Team;
  name: string;
  description: string;
  deadline: Date;
  createdAt: Date;
  tasks: Task[];
}