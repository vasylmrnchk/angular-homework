export interface UpdateProject {
  id: number;
  authorId: number;
  teamId: number;
  name: string;
  description: string;
  deadline: Date;
}