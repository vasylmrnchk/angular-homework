export enum TaskState {
  Created = 0,
  Started = 1,
  Completed = 2,
  Canceled = 3
}