import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { UADatePipe } from './ua-date.pipe';
import { TeamsComponent } from './components/teams/teams.component';
import { ProjectsComponent } from './components/projects/projects.component';
import { UsersComponent } from './components/users/users.component';
import { ProjectAddEditComponent } from './components/project-add-edit/project-add-edit.component';
import { TeamAddEditComponent } from './components/team-add-edit/team-add-edit.component';
import { UserAddEditComponent } from './components/user-add-edit/user-add-edit.component';
import { TaskAddEditComponent } from './components/task-add-edit/task-add-edit.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { DatePipe, registerLocaleData } from '@angular/common';
import localeUA from '@angular/common/locales/uk';
import { DialogService } from './services/dialog.service';
import { TaskStateDirective } from './task-state.directive';

registerLocaleData(localeUA);

@NgModule({
  declarations: [
    AppComponent,
    TeamsComponent,
    ProjectsComponent,
    UsersComponent,
    ProjectAddEditComponent,
    TeamAddEditComponent,
    UserAddEditComponent,
    TaskAddEditComponent,
    PageNotFoundComponent,
    UADatePipe,
    TaskStateDirective
  ],
  imports: [
    BrowserModule, ReactiveFormsModule, AppRoutingModule, HttpClientModule
  ],
  providers: [DatePipe, DialogService],
  bootstrap: [AppComponent]
})
export class AppModule { }
