import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  ActivatedRouteSnapshot
} from '@angular/router';
import { catchError, EMPTY, Observable, of } from 'rxjs';
import { TaskService } from 'src/app/services/task.service';

@Injectable({
  providedIn: 'root'
})
export class TaskResolver implements Resolve<any> {
  constructor(private router: Router, private taskService: TaskService) {}
  resolve(route: ActivatedRouteSnapshot): Observable<any> {
    return this.taskService.get(Number(route.paramMap.get('taskId'))).pipe(catchError(() => {
      this.router.navigate(['/']);
      return EMPTY;
    }));
  }
}