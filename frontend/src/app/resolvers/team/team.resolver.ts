import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  ActivatedRouteSnapshot
} from '@angular/router';
import { catchError, EMPTY, Observable, of } from 'rxjs';
import { TeamService } from 'src/app/services/team.service';

@Injectable({
  providedIn: 'root'
})
export class TeamResolver implements Resolve<any> {
  constructor(private router: Router, private teamService: TeamService) {}
  resolve(route: ActivatedRouteSnapshot): Observable<any> {
    return this.teamService.get(Number(route.paramMap.get('id'))).pipe(catchError(() => {
      this.router.navigate(['/']);
      return EMPTY;
    }));
  }
}
