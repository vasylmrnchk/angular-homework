import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  ActivatedRouteSnapshot
} from '@angular/router';
import { catchError, EMPTY, Observable, of } from 'rxjs';
import { ProjectService } from 'src/app/services/project.service';

@Injectable({
  providedIn: 'root'
})
export class ProjectResolver implements Resolve<any> {
  constructor(private router: Router, private projectService: ProjectService) {}
  resolve(route: ActivatedRouteSnapshot): Observable<any> {
    return this.projectService.get(Number(route.paramMap.get('id'))).pipe(catchError(() => {
      this.router.navigate(['/']);
      return EMPTY;
    }));
  }
}
