import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  ActivatedRouteSnapshot
} from '@angular/router';
import { catchError, EMPTY, Observable, of } from 'rxjs';
import { UserService } from 'src/app/services/user.service';

@Injectable({
  providedIn: 'root'
})
export class UserResolver implements Resolve<any> {
  constructor(private router: Router, private userService: UserService) {}
  resolve(route: ActivatedRouteSnapshot): Observable<any> {
    return this.userService.get(Number(route.paramMap.get('id'))).pipe(catchError(() => {
      this.router.navigate(['/']);
      return EMPTY;
    }));
  }
}