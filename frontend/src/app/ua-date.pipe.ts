import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'uadate'})
export class UADatePipe implements PipeTransform {
  constructor(
    private datePipe: DatePipe
  ) {}

  transform(value: Date, format: string = 'dd MMMM yyyy'): any {
    if (value)
      return this.datePipe.transform(
        value,
        format,
        undefined,
        "uk"
      );
  }
}
 