import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { NewProject } from '../models/project/new-project';
import { Project } from '../models/project/project'; 
import { UpdateProject } from '../models/project/update-project';

const url = `${environment.apiUrl}/api/projects`;

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  constructor(private http: HttpClient) { }

  getAll(): Observable<Project[]> {
    return this.http.get<Project[]>(url);
  }

  get(id: number): Observable<Project> {
    return this.http.get<Project>(`${url}/${id}`);
  }

  create(project: NewProject): Observable<Project> {
    return this.http.post<Project>(url, project);
  }

  update(project: UpdateProject): Observable<any> {
    return this.http.put(url, project);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${url}/${id}`);
  }
}