import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { NewTask } from '../models/task/new-task';
import { Task } from '../models/task/task';
import { UpdateTask } from '../models/task/update-task';

const url = `${environment.apiUrl}/api/tasks`;

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  constructor(private http: HttpClient) { }

  getAll(): Observable<Task[]> {
    return this.http.get<Task[]>(url);
  }

  get(id: number): Observable<Task> {
    return this.http.get<Task>(`${url}/${id}`);
  }

  create(task: NewTask): Observable<Task> {
    return this.http.post<Task>(url, task);
  }

  update(task: UpdateTask): Observable<any> {
    return this.http.put(url, task);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${url}/${id}`);
  }
}