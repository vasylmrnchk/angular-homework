import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Team } from 'src/app/models/team/team';
import { environment } from 'src/environments/environment';
import { NewTeam } from '../models/team/new-team';
import { UpdateTeam } from '../models/team/update-team';


const url = `${environment.apiUrl}/api/teams`;

@Injectable({
  providedIn: 'root'
})
export class TeamService {
  constructor(private http: HttpClient) { }

  getAll(): Observable<Team[]> {
    return this.http.get<Team[]>(url);
  }

  get(id: number): Observable<Team> {
    return this.http.get<Team>(`${url}/${id}`);
  }

  create(team: NewTeam): Observable<Team> {
    return this.http.post<Team>(url, team);
  }

  update(team: UpdateTeam): Observable<any> {
    return this.http.put(url, team);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${url}/${id}`);
  }
}
