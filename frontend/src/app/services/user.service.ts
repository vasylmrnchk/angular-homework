import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { NewUser } from '../models/user/new-user';
import { UpdateUser } from '../models/user/update-user';
import { User } from '../models/user/user';

const url = `${environment.apiUrl}/api/users`;

@Injectable({
  providedIn: 'root'
})

export class UserService {
  constructor(private http: HttpClient) { }

  getAll(): Observable<User[]> {
    return this.http.get<User[]>(url);
  }

  get(id: number): Observable<User> {
    return this.http.get<User>(`${url}/${id}`);
  }

  create(user: NewUser): Observable<User> {
    return this.http.post<User>(url, user);
  }

  update(user: UpdateUser): Observable<any> {
    return this.http.put(url, user);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${url}/${id}`);
  }
}