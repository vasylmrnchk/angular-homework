import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Data, Router } from '@angular/router';
import { first, Observable } from 'rxjs';
import { TaskState } from 'src/app/models/common/taskState';
import { User } from 'src/app/models/user/user';
import { DialogService } from 'src/app/services/dialog.service';
import { TaskService } from 'src/app/services/task.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-task-add-edit',
  templateUrl: './task-add-edit.component.html',
  styleUrls: ['./task-add-edit.component.css']
})
export class TaskAddEditComponent implements OnInit {
  isNewTask: boolean = true;
  submitted: boolean = false;
  taskStates = TaskState;
  performers: User[] | null = [];

  taskForm: FormGroup = this.formBuilder.group({
    id: [''],
    projectId:['', ],
    projectName:['', ],
    performerId:['', [Validators.required]],
    name: ['', [Validators.required, Validators.maxLength(100)]],
    description: ['', [Validators.maxLength(300)]],
    state:['', ],
    finishedAt: ['', ]
  });

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute, 
    private router: Router,
    private taskService: TaskService,
    private userService: UserService,
    private dialogService: DialogService ) { }

  ngOnInit(): void {
    this.getPerformers();
    this.route.data.subscribe((data: Data) => {
      this.taskForm.patchValue({ projectId: data['project'].id });
      this.taskForm.patchValue({ projectName: data['project'].name });
      if (data['task']){
        if(data['project'].id == data['task'].projectId && data['task'].state != this.taskStates.Completed){
          this.taskForm.patchValue(data['task']);
          this.isNewTask = false;
        }else {
          this.router.navigate(['/']);
        }
      }
    });
  }

  getPerformers() {
    this.userService.getAll()
        .pipe(first())
        .subscribe( performers => this.performers = performers);
  }
  
  canDeactivate(): Observable<boolean> | boolean {

    if (!this.submitted && this.taskForm && this.taskForm.dirty) {

        return this.dialogService.confirm(`Discard changes for task #${this.f['id'].value} "${this.f['name'].value}"?`);
    }
    return true;
  }	

  get f() { return this.taskForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.isNewTask) {
      this.createTask();
    } else {
      this.updateTask();
    }
  }

  createTask(){
    this.taskService.create(this.taskForm.value)
          .pipe(first())
          .subscribe({
              next: (response) => {
                this.router.navigate(['projects/']);
                console.log(response);
              },
              error: error => {
                console.log(error);
              }
          });
  }

  updateTask(){
    if (this.f['state'].value == TaskState.Completed){
      this.taskForm.patchValue({ finishedAt: new Date()});
    }
    this.taskService.update(this.taskForm.value)
        .pipe(first())
        .subscribe({
            next: (response) => {
              this.router.navigate(['projects/']);
              console.log("updated");
            },
            error: error => {
              console.log(error);
            }
         });
  }
}

