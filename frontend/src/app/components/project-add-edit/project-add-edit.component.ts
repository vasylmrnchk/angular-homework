import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Data } from '@angular/router';
import { Team } from 'src/app/models/team/team';
import { User } from 'src/app/models/user/user';
import { TeamService } from 'src/app/services/team.service';
import { UserService } from 'src/app/services/user.service';

import { ProjectService } from 'src/app/services/project.service';
import { first, Observable } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DialogService } from 'src/app/services/dialog.service';

@Component({
  selector: 'app-project-add-edit',
  templateUrl: './project-add-edit.component.html',
  styleUrls: ['./project-add-edit.component.css']
})
export class ProjectAddEditComponent implements OnInit {
  isNewProject: boolean = true;
  submitted: boolean = false;
  authors: User[] | null = [];
  teams: Team[] | null = [];

  projectForm: FormGroup = this.formBuilder.group({
    id: [''],
    name: ['', [Validators.required, Validators.maxLength(100)]],
    description: ['', [Validators.maxLength(300)]],
    deadline:['', [Validators.required]],
    authorId: ['', [Validators.required]],
    teamId: ['', [Validators.required]]
  });

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute, 
    private router: Router,
    private projectService: ProjectService,
    private teamService : TeamService, 
    private userService : UserService,
    private dialogService: DialogService) { }

  ngOnInit(): void {
    this.getTeams();
    this.getUsers();
    this.route.data.subscribe((data: Data) => {
      if (data['project']){
          this.projectForm.patchValue(data['project']);
          this.isNewProject = false;
      }
    });
  }

  getUsers() {
    this.userService.getAll()
      .pipe(first())
      .subscribe( users => this.authors = users);
  }

  getTeams() {
    this.teamService.getAll()
        .pipe(first())
        .subscribe( teams => this.teams = teams);
  }

  canDeactivate(): Observable<boolean> | boolean {

    if (!this.submitted && this.projectForm && this.projectForm.dirty) {

        return this.dialogService.confirm(`Discard changes for project #${this.f['id'].value} "${this.f['name'].value}"?`);
    }
    return true;
  }	

  get f() { return this.projectForm.controls; }
  
  onSubmit() {
    this.submitted = true;
    if (this.isNewProject) {
      this.createProject();
    } else {
      this.updateProject();
    }
  }

  createProject(){
    this.projectService.create(this.projectForm.value)
          .pipe(first())
          .subscribe({
              next: (response) => {
                this.router.navigate(['../'], {relativeTo: this.route});
                console.log(response);
              },
              error: error => {
                console.log(error);
              }
          });
  }

  updateProject(){
    this.projectService.update(this.projectForm.value)
        .pipe(first())
        .subscribe({
            next: (response) => {
              this.router.navigate(['../'], {relativeTo: this.route});
            },
            error: error => {
              console.log(error);
            }
          });
  }
}
