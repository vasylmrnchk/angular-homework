import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Data, Router } from '@angular/router';
import { first, Observable } from 'rxjs';
import { Team } from 'src/app/models/team/team';
import { DialogService } from 'src/app/services/dialog.service';
import { TeamService } from 'src/app/services/team.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-add-edit',
  templateUrl: './user-add-edit.component.html',
  styleUrls: ['./user-add-edit.component.css']
})
export class UserAddEditComponent implements OnInit {
  teams: Team[] | null = [];
  isNewUser: boolean = true;
  submitted = false;

  userForm: FormGroup = this.formBuilder.group({
    id: [''],
    teamId:['', [Validators.required]],
    firstName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(26)]],
    lastName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(30)]],
    email: ['', [Validators.required, Validators.email]],
    birthDay: ['', [Validators.required]]
  });

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute, 
    private router: Router,
    private teamService : TeamService, 
    private userService : UserService,
    private dialogService: DialogService) { }

  ngOnInit(): void {
    this.getTeams();
    ///
    this.route.data.subscribe((data: Data) => {
      if (data['user']){
          this.userForm.patchValue(data['user']);
          this.isNewUser = false;
      }
    });
  }

  getTeams() {
    this.teamService.getAll()
        .pipe(first())
        .subscribe( teams => this.teams = teams);
  }
  
  canDeactivate(): Observable<boolean> | boolean {

    if (!this.submitted && this.userForm && this.userForm.dirty) {

        return this.dialogService.confirm('Discard changes for User?');
    }
    return true;
  }	

  get f() { return this.userForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.isNewUser) {
      this.createUser();
    } else {
      this.updateUser();
    }
  }

  createUser(){
    this.userService.create(this.userForm.value)
          .pipe(first())
          .subscribe({
              next: (response) => {
                this.router.navigate(['../'], {relativeTo: this.route});
                console.log(response);
              },
              error: error => {
                console.log(error);
              }
          });
  }

  updateUser(){
    this.userService.update(this.userForm.value)
        .pipe(first())
        .subscribe({
            next: (response) => {
              this.router.navigate(['../'], {relativeTo: this.route});
              console.log("updated");
            },
            error: error => {
              console.log(error);
            }
          });
  }
}
