import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute, Data, Router } from '@angular/router';
import { first, Observable } from 'rxjs';
import { DialogService } from 'src/app/services/dialog.service';
import { TeamService } from 'src/app/services/team.service';

@Component({
  selector: 'app-team-add-edit',
  templateUrl: './team-add-edit.component.html',
  styleUrls: ['./team-add-edit.component.css']
})
export class TeamAddEditComponent implements OnInit {
  submitted: boolean = false;
  isNewTeam: boolean = true;
  taskForm: FormGroup  = this.formBuilder.group({
    id:[''],
    name:['', Validators.required],
  });
  
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute, 
    private router: Router,
    private teamService : TeamService,
    private dialogService: DialogService) { }

  ngOnInit(): void {
    ///
    this.route.data.subscribe((data: Data) => {
      if (data['team']){
          this.taskForm.patchValue(data['team']);
          this.isNewTeam = false;
      }
    });
  }

  canDeactivate(): Observable<boolean> | boolean {

    if (!this.submitted && this.taskForm && this.taskForm.dirty) {

        return this.dialogService.confirm(`Discard changes for team #${this.f['id'].value} "${this.f['name'].value}"?`);
    }
    return true;
  }	

  get f() { return this.taskForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.isNewTeam) {
      this.createTeam();
    } else {
      this.updateTeam();
    }
  }

  createTeam(){
    this.teamService.create(this.taskForm.value)
          .pipe(first())
          .subscribe({
              next: (response) => {
                this.router.navigate(['../'], {relativeTo: this.route});
                console.log(response);
              },
              error: error => {
                console.log(error);
              }
          });
  }

  updateTeam(){
    this.teamService.update(this.taskForm.value)
        .pipe(first())
        .subscribe({
            next: (response) => {
              this.router.navigate(['../'], {relativeTo: this.route});
              console.log("updated");
            },
            error: error => {
              console.log(error);
            }
          });
  }
}

