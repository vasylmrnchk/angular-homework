import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs';
import { Project } from 'src/app/models/project/project';
import { ProjectService } from 'src/app/services/project.service';
import { Task } from 'src/app/models/task/task';
import { TaskState } from 'src/app/models/common/taskState';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {
  projects: Project[] | null = [];
  selectedProject?: Project;
  taskStates = TaskState;
  selectedTask?: Task;

  constructor(private projectService: ProjectService) { }

  ngOnInit(): void {
    this.getProjects();
  }

  getProjects(): void {
    this.projectService.getAll()
        .pipe(first())
        .subscribe( projects => this.projects = projects);
  }
  
  onSelect(project: Project): void {
    this.selectedProject = project;
    this.selectedTask = undefined;
  }

  onSelectTask(task: Task): void {
    this.selectedTask = task;
  }
}
