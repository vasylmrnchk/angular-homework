import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs';
import { Team } from 'src/app/models/team/team';
import { TeamService } from 'src/app/services/team.service';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css']
})

export class TeamsComponent implements OnInit {
  teams: Team[] | null = [];
  selectedTeam?: Team;
  
  constructor(private teamService: TeamService) { }

  ngOnInit(): void {
    this.getTeams();
  }

  getTeams() {
    this.teamService.getAll()
        .pipe(first())
        .subscribe( teams => this.teams = teams);
  }

  onSelect(team: Team): void {
    this.selectedTeam = team;
  }
}
