import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs';
import { User } from 'src/app/models/user/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: User[] | null = [];
  selectedUser?: User;
  
  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.getUsers();
  }

  getUsers() {
    this.userService.getAll()
        .pipe(first())
        .subscribe( users => this.users = users);
  }

  onSelect(user: User): void {
    this.selectedUser = user;
  }
}
