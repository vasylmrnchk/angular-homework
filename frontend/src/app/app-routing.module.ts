import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProjectsComponent } from './components/projects/projects.component'; 
import { UsersComponent } from './components/users/users.component'; 
import { TeamsComponent } from './components/teams/teams.component';
import { ProjectAddEditComponent } from './components/project-add-edit/project-add-edit.component';
import { TeamAddEditComponent } from './components/team-add-edit/team-add-edit.component';
import { UserAddEditComponent } from './components/user-add-edit/user-add-edit.component';
import { TaskAddEditComponent } from './components/task-add-edit/task-add-edit.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { ProjectResolver } from './resolvers/project/project.resolver';
import { TaskResolver } from './resolvers/task/task.resolver';
import { UserResolver } from './resolvers/user/user.resolver';
import { TeamResolver } from './resolvers/team/team.resolver';
import { CanDeactivateGuard } from './guards/can-deactivate.guard';

const routes: Routes = [
  { path: '', redirectTo: '/projects', pathMatch: 'full' },
  { path: 'projects', component: ProjectsComponent },
  { path: 'projects/add', component: ProjectAddEditComponent },
  { path: 'projects/:id', 
    component: ProjectAddEditComponent, 
    resolve: { project: ProjectResolver },
    canDeactivate: [CanDeactivateGuard]},
  { 
    path: 'projects/:id/tasks', 
    resolve: { project: ProjectResolver },
    children: [
      { path: 'add', component: TaskAddEditComponent},
      { 
        path: ':taskId', 
        component: TaskAddEditComponent, 
        resolve: { task: TaskResolver },
        canDeactivate: [CanDeactivateGuard]
      }      
    ]
  },
  { path: 'users', component: UsersComponent },
  { path: 'users/add', component: UserAddEditComponent },
  { path: 'users/:id', 
    component: UserAddEditComponent, 
    resolve: { user: UserResolver }, 
    canDeactivate: [CanDeactivateGuard]},
  { path: 'teams', component: TeamsComponent },
  { path: 'teams/add', component: TeamAddEditComponent 
  },
  { path: 'teams/:id', 
    component: TeamAddEditComponent, 
    resolve: { team: TeamResolver }, 
    canDeactivate: [CanDeactivateGuard]
  },

  { path: '**', pathMatch: 'full', component: PageNotFoundComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ],
  providers: [CanDeactivateGuard]
})
export class AppRoutingModule {}